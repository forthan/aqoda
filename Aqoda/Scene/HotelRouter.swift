//
//  HotelRouter.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

protocol HotelRouterInterface {
  func navigateToViewController()
}

final class HotelRouter: HotelRouterInterface {
  weak var viewController: HotelViewController!

  // MARK: - Navigation

    func navigateToViewController() {
        // TODO: Navigate to viewController
    }
}
