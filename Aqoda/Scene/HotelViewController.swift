//
//  HotelViewController.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

protocol HotelView: NSObjectProtocol {
    func displayTextView(display: HotelModels.ViewModel.Displayed)
}

class HotelViewController: UIViewController {
    var interactor: HotelInteractorBusinessLogic!
    var router: HotelRouterInterface!
    
    @IBOutlet weak var textView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
    
        HotelConfigurator.sharedInstance.configure(viewController: self)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.startGetInput(request: HotelModels.Request())
    }
}
// MARK: - HotelViewController
extension HotelViewController: HotelView {
    func displayTextView(display: HotelModels.ViewModel.Displayed) {
        self.textView.text.append(display.string + "\n")
    }
    
}

