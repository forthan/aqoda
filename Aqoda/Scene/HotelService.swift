//
//  HotelService.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

class HotelService {
    // MARK: - Calling API Manager
    func getData(url: String = Constants.defaultUrl, success: @escaping ([Action]) -> (), failure: @escaping (_ error: String?) -> ()) {
        let api = APIManager()
        api.getInput(url: url, fail: { (error) in
            failure(error?.localizedDescription)
        }) { (result) in
            success(result)
        }
    }
}
