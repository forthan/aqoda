//
//  HotelPresenter.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

protocol HotelPresenterInterface {
    func presentTextView(response: HotelModels.Response)
}

class HotelPresenter: HotelPresenterInterface {
    var viewController: HotelView!
    func presentTextView(response: HotelModels.Response) {
        switch response.action {
        case .created:
            let hotel = response.data as? Hotel
            viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Hotel created with  \(hotel?.floor ?? 0) floor(s), \(hotel?.roomPerFloor ?? 0) room(s) per floor."))
        case .booked:
            if let room = response.data as? Room {
                viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Room \(room.roomNo ?? 0) is booked by \(room.bookedBy?.name ?? "") with keycard number \(room.keycard ?? 1)"))
                
            }else if let rooms = response.data as? [Room] {
                showBookedFloor(rooms)
            }
        case .cannotBook:
            let room = response.option as? Room
            viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Cannot book room \(room?.roomNo ?? 0) for \(response.data ?? ""), The room is currently booked by \(room?.bookedBy?.name ?? "")"))
        case .availableRooms:
            guard let rooms = response.data as? [Room] else { return  }
            for room in rooms {
                viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "\(room.roomNo ?? 0)"))
            }
        case .checkout:
            if let room = response.data as? Room {
                viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Room \(room.roomNo ?? 0) is checkout"))
            }else if let string = response.data as? String {
                 viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Room \(string) are checkout"))
            }
        case .cannotCheckout:
            let room = response.data as? Room
            viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Only \(room?.bookedBy?.name ?? "") can checkout with keycard number \(room?.keycard ?? 0)"))
        case .guestList:
            guard let room = response.data as? [Room] else { return  }
            guestList(rooms: room)
        case .cannotBookFloor:
            let room = response.option as? Room
            viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Cannot book floor \(room?.floor ?? 0) for \(response.data ?? "")"))
        default:
            viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: response.action.map { $0.rawValue }))
            
        }
        
    }
    
    func guestList(rooms: [Room]) {
        var guest = ""
        for room in rooms {
            if !guest.isEmpty {
                guest += ", " + room.bookedBy!.name
            }else {
                guest += room.bookedBy!.name
            }
        }
        viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: guest))
    }
    
    func showBookedFloor(_ rooms: [Room]) {
        var roomString = ""
        var keyCard = ""
        for room in rooms {
            roomString += roomString.isEmpty ? "\(room.roomNo ?? 0)" : ", \(room.roomNo ?? 0)"
            keyCard += keyCard.isEmpty ? "\(room.keycard ?? 0)" : ", \(room.keycard ?? 0)"
        }
        viewController.displayTextView(display: HotelModels.ViewModel.Displayed(string: "Room \(roomString) are booked with keycard number \(keyCard)"))
    }
    
    
}

