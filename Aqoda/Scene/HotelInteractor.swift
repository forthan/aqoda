//
//  HotelInteractor.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

protocol HotelInteractorBusinessLogic {
    func startGetInput(request: HotelModels.Request)
}

protocol HotelInteractorDataStore{
    var hotel: Hotel! { get }
}

final class HotelInteractor: HotelInteractorBusinessLogic, HotelInteractorDataStore {
    var presenter: HotelPresenterInterface!
    var hotel: Hotel!
    var booked: [Room] = []
    var available: [Int] = []
    var worker: HotelWorker = HotelWorker(with: HotelService())
    
    // MARK: - Business logic
    
    func startGetInput(request: HotelModels.Request) {
        worker.getInput(url: request.url) { (response) in
            for action in response.actions {
                switch action.key {
                case "create_hotel":
                    self.hotel = self.createHotel(value: action.value as! [Any])
                case "book":
                    self.book(value: action.value as! [Any])
                case "list_available_rooms":
                    self.checkAvailableRooms()
                case "checkout":
                    self.checkout(data: action.value as! [Any])
                case "list_guest":
                    self.guestList()
                case "get_guest_in_room":
                    let data = action.value as? [Any]
                    self.guestList(room: (data?.first as! String))
                case "list_guest_by_age":
                    let data = action.value as? [String]
                    self.guestList(symbol: data?.first ?? "", age: Int(data?[1] ?? "") ?? 0)
                case "list_guest_by_floor":
                     let data = action.value as? [String]
                     self.guestList(floor: Int(data?.first ?? "") ?? 0)
                case "checkout_guest_by_floor":
                    let data = action.value as? [String]
                    self.checkout(floor: Int(data?.first ?? "") ?? 0)
                case "book_by_floor":
                      let data = action.value as? [Any]
                      self.bookFloor(value: data!)
                    
                default:
                    break
                }
               
            }
            
        }
    }
    
    func createHotel(value: [Any]) -> Hotel {
        guard let array = value as? [String], let floor = Int(array[0]), let room = Int(array[1]) else {
            return Hotel(floor: 0, roomPerFloor: 0)
        }
        for card in 1...(floor * room) {
            available.append(card)
            
        }
        let hotel = Hotel.init(floor: floor, roomPerFloor: room)
         presenter.presentTextView(response: HotelModels.Response(action: ActionType.created, data: hotel))
        return hotel
    }
    
    func book(value: [Any]) {
        guard let data = value as? [String], let room = Int(data[0]), let guest = data[1] as? String, let age = Int(data[2]) else {
            return
        }
        
        let bookedRoom = self.booked.filter{$0.roomNo == room}.first
        if (bookedRoom == nil) {
            let bookingRoom = self.hotel?.rooms.filter{$0.roomNo == room}.first
            let bookBy = Guest()
            bookBy.name = guest
            bookBy.age = age
            bookingRoom?.bookedBy = bookBy
            bookingRoom?.keycard = findKeyCard()
            booked.append(bookingRoom!)
            presenter.presentTextView(response: HotelModels.Response(action: .booked, data: bookingRoom))
            print("booked \(room)")
        }else {
            print("Cannot book room \(room)")
            presenter.presentTextView(response: HotelModels.Response(action: .cannotBook, data: guest, option: bookedRoom))
        }
        
    }
    
    func bookFloor(value: [Any]) {
        guard let data = value as? [String], let floor = Int(data[0]), let guest = data[1] as? String, let age = Int(data[2]) else {
            return
        }
        
        let rooms = hotel.rooms.filter{$0.floor == floor}
        for room in rooms {
            let bookedRoom = self.booked.filter{$0.roomNo == room.roomNo}.first
            if (bookedRoom == nil) {
                let bookBy = Guest()
                bookBy.name = guest
                bookBy.age = age
                room.bookedBy = bookBy
                room.keycard = findKeyCard(floor: true)
                booked.append(room)
                print("booked \(room)")
            }else {
                print("Cannot book room \(room)")
                presenter.presentTextView(response: HotelModels.Response(action: .cannotBookFloor, data: guest, option: bookedRoom))
                return
            }
            
        }
        presenter.presentTextView(response: HotelModels.Response(action: .booked, data: rooms))

    }

    func findKeyCard(floor: Bool = false) -> Int {
        let availableCard = available[0]
        available.remove(at: 0)
        return availableCard
    }
    
    func checkAvailableRooms() {
        let availableRooms = hotel?.rooms.filter{$0.bookedBy == nil}
        if availableRooms!.isEmpty {
             presenter.presentTextView(response: HotelModels.Response(action: .roomNotavailable))
        }else {
            presenter.presentTextView(response: HotelModels.Response(action: .availableRooms, data: availableRooms))
        }
       
    }
    
    func checkout(data: [Any]) {
        let keyCard = Int(data.first as! String)
        let room = hotel?.rooms.filter{$0.keycard == keyCard}.first
        if room?.bookedBy?.name == (data[1] as? String) {
            available.append(keyCard!)
            presenter.presentTextView(response: HotelModels.Response(action: .checkout, data: room))
            checkout(room!)
        }else {
            presenter.presentTextView(response: HotelModels.Response(action: .cannotCheckout, data: room))
        }
        
    }
    
    func checkout(floor: Int) {
        let rooms = booked.filter{$0.floor == floor}
        var string = ""
        for room in rooms {
            string += string.isEmpty ? "\(room.roomNo ?? 0)": ", \(room.roomNo ?? 0)"
            available.append(room.keycard!)
            checkout(room)
        }
        
        presenter.presentTextView(response: HotelModels.Response(action: .checkout, data: string))
        
    }
    
    func checkout(_ room: Room) {
        room.keycard = nil
        room.bookedBy = nil
        booked.removeAll{$0.roomNo == room.roomNo}
    }
    
    func guestList(room: String? = nil) {
        if room != nil {
            presenter.presentTextView(response: HotelModels.Response(action: .guestList, data: booked.filter{$0.roomNo == Int(room!)}))
        }else {
            presenter.presentTextView(response: HotelModels.Response(action: .guestList, data: booked))
        }
    }
    
    func guestList(symbol: String, age: Int) {
        switch symbol {
        case "<":
            presenter.presentTextView(response: HotelModels.Response(action: .guestList, data: booked.filter{$0.bookedBy!.age < age}))
        case ">":
            presenter.presentTextView(response: HotelModels.Response(action: .guestList, data: booked.filter{$0.bookedBy!.age > age}))
        default:
            break
        }
       
    }
    
    func guestList(floor: Int) {
        presenter.presentTextView(response: HotelModels.Response(action: .guestList, data: booked.filter{$0.floor == floor}))
       }
}

