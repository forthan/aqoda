//
//  HotelWorker.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

protocol HotelWorkerInterface {
  func getSomething(aQueryString: String, aCompletion: @escaping (Any) -> Void)
}

final class HotelWorker {

    var service: HotelService!
    
    init(with aService: HotelService) {
        service = aService
    }

  // MARK: - Business Logic

    func getInput(url: String?, completion: @escaping (HotelModels.DataResponse) -> Void) {
        service.getData(success: { (result) in
            completion(HotelModels.DataResponse(actions: result))
        }) { (error) in
            
        }
    }
}
