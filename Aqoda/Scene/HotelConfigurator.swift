//
//  HotelConfigurator.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright (c) 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import UIKit

final class HotelConfigurator {

  // MARK: - Object lifecycle

  static let sharedInstance = HotelConfigurator()

  // MARK: - Configuration

  func configure(viewController: HotelViewController) {
    let router = HotelRouter()
    router.viewController = viewController

    let presenter = HotelPresenter()
    presenter.viewController = viewController

    let interactor = HotelInteractor()
    interactor.presenter = presenter

    viewController.interactor = interactor
    viewController.router = router
  }
}
