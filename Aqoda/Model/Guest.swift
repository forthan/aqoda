//
//  Guest.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import Foundation

class Guest {
    var name: String!
    var age: Int!
}
