//
//  Room.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import Foundation

class Room {
    var roomNo: Int!
    var floor: Int!
    var bookedBy: Guest?
    var keycard: Int?
    
    func roomNumber(floor: Int, room: Int){
        roomNo = (floor * 100) + room
        
    }
}
