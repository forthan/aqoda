//
//  Hotel.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import Foundation

struct Hotel {
    var floor: Int!
    var roomPerFloor: Int!
    var rooms: [Room] = []
    
    init(floor: Int, roomPerFloor: Int) {
        self.floor = floor
        self.roomPerFloor = roomPerFloor
        for f in 1...floor {
            for r in 1...roomPerFloor {
                let room = Room()
                room.floor = f
                room.roomNumber(floor: f, room: r)
                rooms.append(room)
            }
        }
        
    }

    
}


