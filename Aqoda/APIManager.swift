//
//  APIManager.swift
//  Aqoda
//
//  Created by Thanyarat Pothong Thaosriwich on 2019/11/14.
//  Copyright © 2019 Thanyarat Pothong Thaosriwich. All rights reserved.
//

import Foundation
import Alamofire
struct Constants {
    static let defaultUrl: String = "https://gitlab.com/sittipong-codeapp/aqoda/raw/develop/input.txt"
}

class APIManager {
    func getInput(url: String, fail: @escaping (_ error: Error?) -> (),
                  success: @escaping (_ data: [Action]) -> ()) {
        AF.request(url).response { (response) in
            
            if let data = response.data, let utf8String = String(data: data, encoding: .utf8) {
                let result = self.handleData(utf8String: utf8String)
                success(result)
            }else {
                fail(response.error)
            }
        }
    }
    
    func handleData(utf8String: String) -> [Action]{
        let array = utf8String.components(separatedBy: "\n")
        var result: [Action] = []
        for item in array {
            var value = item.components(separatedBy: " ")
            let key = value.first
            value.remove(at: 0)
            result.append(Action(key: key, value: value))
            
        }
        return result
    }
        
        
}
